# NextCloud

Ce dossier contient les ressources nécessaires les instances NextCloud hébergées par Picasoft.
Il y a une instance par dossier ; les instructions ci-dessous sont à exécuter à l'intérieur de chaque dossier.

Voir également le [wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:nextcloud:start).

[[_TOC_]]

## Configuration

Quasiment aucune configuration n'est effectuée via les fichiers de ce dépôt, et on préfère l'interface web.
Le défaut est qu'il n'est pas possible de lancer des instances NextCloud **vraiment** personnalisées depuis ce dépôt, mais c'est parce que le format des fichiers de configuration est amené à évoluer et que NextCloud effectue des migrations automatiques lors des mises à jour.

Versionner les fichiers de configuration serait donc en conflit avec les modifications automatiques effectuées par NextCloud lors des mises à jour et des changements dans l'interface.

Les fichiers `nginx.conf` sont repris de [cet exemple](https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/with-nginx-proxy/postgres/fpm/web/nginx.conf).

Il est possible que NextCloud râle à propos de la configuration manquante `default_phone_region`, qui se trouve normalement dans le fichier `config.php` qui n’est ici pas modifié. Dans ce cas, exécuter la commande suivante :

```bash
docker exec --user www-data -it <nom-du-conteneur> php occ config:system:set default_phone_region --value="FR"
```

## Lancement

Copier les fichiers `.secrets.example` en `.secrets` et remplacer les valeurs.
Lancer `docker-compose up -d`.

## Mise en place du cron

Nextcloud a besoin de lancer régulièrement des tâches en fond.
Pour cela, on lance une commande à l'intérieur du conteneur depuis l'extérieur, avec les timers systemd (équivalents à cron)

Voici les commandes qui permettent de l'installer :

```bash
# pas possible d’utiliser ln ici car le volume du dépôt n’est pas encore monté lorsque systemd scanne les services.
sudo find . -regex '*.service' -o -regex '*.timer' -exec cp $(realpath {}) /etc/systemd/system/ \;
sudo systemctl daemon-reload
sudo find . -regex '*.timer' -exec systemctl enable --now {} \;
```

- On copie les fichiers `*.service` et `*.timer` présents dans ce dossier. **Il est nécessaire de réaliser cette copie à chaque mise-à-jour du dépôt.**
- On dit à `systemd` de relire ses dossiers, pour qu'il prenne en compte l'existence de ces nouveaux fichiers.
- On dit à `systemd` d'activer au démarrage et de lancer le *timer* systemd.

Toutes les 5 minutes, le timer lancera le service.

Pour plus d'infos, voir [la doc nextcloud](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/background_jobs_configuration.html#systemd),
et les pages de manuel `systemd.timer` et `systemd.service`.

## Mise à jour de Nextcloud

Pour mettre à jour l'instance de Picasoft, il suffit de mettre à jour le tag de l'image officielle de NextCloud.

Attention : **toutes les mises à jour de version majeure doivent se faire une par une**. Les logs applicatifs détaillent la mise à jour.
Exemple :
* 15 -> 16, puis
* 16 -> 17, puis
* 17 -> 18.

Sinon, il y a risque de casse.

Bien vérifier ensuite sur la page d’accueil de l’administration (`/settings/admin/overview`) qu’il n’y a pas des migrations non-essentielles à réaliser manuellement (généralement via le scipt PHP `occ`).

Attention à bien vérifier que la BDD est prête avant de démarrer le conteneur Nextcloud pour effectuer la migration, si celui-ci ne peut pas accéder à sa BDD, le script de migration va planter et démarrer Nextcloud (qui ne pourra pas fonctionner correctement).
Dans ce cas, redémarrer le conteneur ne relancera pas le script de migration, il faut alors exécuter manuellement la commande `php occ upgrade` dans le conteneur.

## Mises à jour des bases de données

### Picasoft : mise à jour de MariaDB

[Selon la documentation](https://mariadb.com/kb/en/upgrading-between-major-mariadb-versions/) :

> MariaDB is designed to allow easy upgrades. You should be able to trivially upgrade from ANY earlier MariaDB version to the latest one (for example MariaDB 5.5.x to MariaDB 10.5.x), usually in a few seconds.

L'idée est d'éteindre le conteneur applicatif (NextCloud), puis de lancer la nouvelle version du conteneur, d'entrer dedans, de lancer la commande `mariadb_upgrade` et de redémarrer le conteneur.
Il est aussi possible de demander à la BDD d’exécuter automatiquement la mise-à-niveau si besoin en ajoutant la variable d’environnement `MARIADB_AUTO_UPGRADE=1`.

### Solidaires : mise à jour de Postgres

tl;dr c'est chiant, il n'y a pas d'upgrade automatique dans Docker, cf [cette issue](https://github.com/docker-library/postgres/issues/37).

* Soit on fait [comme Mattermost](https://gitlab.utc.fr/picasoft/projets/services/mattermost#mise-%C3%A0-jour-du-sgbd) (i.e. dump puis restore)
* Soit on tente une [méthode Docker-like](https://github.com/tianon/docker-postgres-upgrade) maintenu par un bénévole. Dans tous les cas [faire un backup](https://wiki.picasoft.net/doku.php?id=technique:adminsys:backup:start)...

## Modifications spéciales

Cette section a pour objectif de consigner les modifications faite à l'intérieur du conteneur, au niveau des apps, etc, pas tant pour pouvoir les exécuter de nouveau (elles sont backupées) que pour comprendre comment on a résolu tel problème, comment on en est arrivé là, etc.

### Solidaires

Une instance minimale où on a désactivé la plupart des apps d'accueil, de statut, de météo etc. L'idée est juste de permettre du partage facile de fichier, un calendrier et de l'édition collaborative.
Ainsi, on utilise le plugin Collabora ainsi qu'une instance Collabora qui pourra être ré-utilisée par la suite, voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:nextcloud:collabora) pour plus de détails.

On a aussi supprimé les fichiers par défaut pour chaque utilisateur·ces, qui créent plus de confusion qu'autre chose. Pour ce faire, exécuter :

```
docker exec -u www-data -it <app> php occ config:system:set skeletondirectory --value=''
```
